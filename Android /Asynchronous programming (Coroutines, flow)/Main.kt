import kotlinx.coroutines.*
import java.net.HttpURLConnection
import java.net.URL

fun main() = runBlocking {
    val websites = listOf(
        "https://www.google.com",
        "https://www.facebook.com",
        "https://www.github.com",
        "https://www.twitter.com",
        "https://www.instagram.com" ,
        "https://www.youtube.com/",
        "https://gitlab.com/",
        "https://www.kinopoisk.ru/",
        "https://vk.com/",
        "https://www.metacritic.com/"
    )

    val checkJobs = websites.map { url ->
        async(Dispatchers.IO) {
            checkWebsite(url)
        }
    }

    checkJobs.forEachIndexed { index, deferred ->
        val url = websites[index]
        val isAvailable = deferred.await()
        println("Сайт $url ${if (isAvailable) "доступен" else "недоступен"}")
    }
}

fun checkWebsite(url: String): Boolean {
    return try {
        val connection = URL(url).openConnection() as HttpURLConnection
        connection.requestMethod = "HEAD"
        connection.connectTimeout = 5000
        connection.readTimeout = 5000
        connection.responseCode == HttpURLConnection.HTTP_OK
    } catch (e: Exception) {
        false
    }
}

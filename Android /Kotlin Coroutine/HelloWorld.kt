import kotlinx.coroutines.*


val myScope = CoroutineScope(Dispatchers.Default + Job())

fun main() {
    myScope.launch {
        delay(1000)
        println("World")
    }

    println("Hello,")
    Thread.sleep(2000)
    myScope.cancel()
}
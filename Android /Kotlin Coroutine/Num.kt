import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun function1(): Int {
    delay(1000) // Имитация долгой операции
    return 10
}

suspend fun function2(): Int {
    delay(1000) // Имитация долгой операции
    return 20
}

fun main() = runBlocking {
    // Последовательный вызов
    val timeSequential = measureTimeMillis {
        val result1 = function1()
        val result2 = function2()
        val sumSequential = result1 + result2
        println("Последовательный вызов, сумма: $sumSequential")
    }
    println("Время выполнения последовательного вызова: $timeSequential мс\n")

    // Асинхронный вызов
    val timeAsync = measureTimeMillis {
        val deferred1 = async { function1() }
        val deferred2 = async { function2() }
        val sumAsync = deferred1.await() + deferred2.await()
        println("Асинхронный вызов, сумма: $sumAsync")
    }
    println("Время выполнения асинхронного вызова: $timeAsync мс")
}
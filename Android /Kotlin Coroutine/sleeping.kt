import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun function1(): Int {
    delay(1000)
    return 10
}

suspend fun function2(): Int {
    delay(1000)
    return 20
}

suspend fun printSleepMessages() {
    repeat(3) { i ->
        println("I'm sleeping $i ...")
        delay(1000)
    }
}

fun main() = runBlocking {
    // Последовательный вызов
    println("Последовательный вызов:")
    val timeSequential = measureTimeMillis {
        val result1 = function1()
        val result2 = function2()
        val sumSequential = result1 + result2
        println("Сумма: $sumSequential")
        printSleepMessages()
    }
    println("Время выполнения последовательного вызова: $timeSequential мс\n")
    println("main: I'm tired of waiting! I'm running finally")
    println("main: Now I can quit.\n")

    // Асинхронный вызов
    println("Асинхронный вызов:")
    val timeAsync = measureTimeMillis {
        val deferred1 = async { function1() }
        val deferred2 = async { function2() }
        val sumAsync = deferred1.await() + deferred2.await()
        println("Сумма: $sumAsync")
    }
    printSleepMessages()
    println("Время выполнения асинхронного вызова: $timeAsync мс\n")
    println("main: I'm tired of waiting! I'm running finally")
    println("main: Now I can quit.")
}
